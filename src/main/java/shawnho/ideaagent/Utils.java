package shawnho.ideaagent;

import java.nio.ByteBuffer;

/**
 * @author shawnho
 */
public class Utils {
    public static byte[] getObstructResult() {
        ByteBuffer buffer = ByteBuffer.allocate(14);
        buffer.putLong(System.currentTimeMillis());
        buffer.put(new byte[]{1, 2, 3, 4, 5, 6});
        return buffer.array();
    }

    public static byte[] getRealityResult() {
        ByteBuffer buffer = ByteBuffer.allocate(14);
        buffer.putLong(System.currentTimeMillis());
        buffer.put(new byte[]{1, 1, 0, 3, 6, 5});
        return buffer.array();
    }
}
